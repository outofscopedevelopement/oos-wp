<?php get_header(); ?>

	<!-- oos default for pages -->
	<div id="content_container">
    <?php
      $navigation = "";
      $pages = get_pages();
      $id = 0;

      foreach ($pages as $page_data) {
        $image_url = wp_get_attachment_url( get_post_thumbnail_id( $page_data->ID ), 'post-thumbnail' );
        $content = apply_filters('the_content', $page_data->post_content);
        $title = $page_data->post_title;
        echo '<div id="content_'.$id.'" class="content" style="background-image: url('.$image_url.')"><h2 id="content_'.$id.'_heading">'.$title.'</h2><div class="text_box">'.$content.'</div></div>';
        $navigation = $navigation.'<li class="list_element" id="list_element_'.$id.'" ><div class="link_title"><a> '.$title.' </a></div><div class="icon_container"><div class="icon" id="link_'.$id.'"></div></div></li>';
        $id += 1;
      }

      echo '<div id="navigation"><ul>'.$navigation.'</ul></div>'
    ?>

	</div> <!-- end #content -->

<?php get_footer(); ?>

